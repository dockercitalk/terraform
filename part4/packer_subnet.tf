# Create a subnet to launch packer builds into.
resource "aws_subnet" "packer_subnet" {
  vpc_id = "${aws_vpc.slides_vpc.id}"
  cidr_block = "10.0.0.0/24"
  map_public_ip_on_launch = true
  tags {
    Name = "packer_subnet"
  }
}
