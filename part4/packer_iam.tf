resource "aws_iam_user" "packer_user" {
  name = "packer"
  path = "/"
  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_iam_group" "packer_group" {
  name = "packer"
  path = "/"
}

resource "aws_iam_user_group_membership" "packer_membership" {
  user = "${aws_iam_user.packer_user.name}"
  groups = ["${aws_iam_group.packer_group.name}"]
}

data "template_file" "packer_policy_template" {
  template = "${file("templates/packer_policy.tpl")}"
}

resource "aws_iam_policy" "packer_policy" {
  name = "packer"
  path = "/"
  description = "Grants the minimum permissions needed for packer to work."
  policy = "${data.template_file.packer_policy_template.rendered}"
}

resource "aws_iam_group_policy_attachment" "attach_packer_policy_to_group" {
  group = "${aws_iam_group.packer_group.name}"
  policy_arn = "${aws_iam_policy.packer_policy.arn}"
}
