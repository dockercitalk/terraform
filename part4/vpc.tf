# A VPC is a overarching network in AWS.
resource "aws_vpc" "slides_vpc" {
  cidr_block = "10.0.0.0/16"
  tags {
    Name = "Dockerslides VPC"
  }
}

# An internet gateway how public IP servers in AWS connect to the internet.
resource "aws_internet_gateway" "slides_igate" {
  vpc_id = "${aws_vpc.slides_vpc.id}"
  tags {
    Name = "Dockerslides igateway"
  }
}

# We establish a route to pass outgoing internet traffic to the internet gateway.
resource "aws_route" "slides_iaccess" {
  route_table_id = "${aws_vpc.slides_vpc.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = "${aws_internet_gateway.slides_igate.id}"
}
