# Remote state storage
terraform {
  backend "consul" {
    path = "dockerslides"
  }
}

provider "aws" {
  region = "us-east-1"
}

data "aws_caller_identity" "current" {}
