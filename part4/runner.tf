data "aws_ami" "runner_ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["runner-*"]
  }

  # Current account ID.
  owners = ["${data.aws_caller_identity.current.account_id}"]
}

resource "aws_subnet" "runner_sub" {
  vpc_id = "${aws_vpc.slides_vpc.id}"
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = true
  tags {
    Name = "dockerslides runner subnet"
  }
}

resource "aws_security_group" "runner_sg" {
  name = "dockerslides_runner"
  description = "Dockerslides gitlab runner SG"
  vpc_id = "${aws_vpc.slides_vpc.id}"

  tags {
    Name = "Dockerslides gitlab runner"
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_launch_configuration" "gitlabrunner_lc" {
  name_prefix = "dockerslides_runner"
  image_id = "${data.aws_ami.runner_ami.id}"
  instance_type = "t2.medium"
  # You might need to change this if your AWS ssh key is called somethin else.
  key_name = "docker_slides"
  security_groups = ["${aws_security_group.runner_sg.id}"]

  root_block_device {
    volume_type = "gp2"
    volume_size = "50"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "gitlabrunner_autog" {
  name = "dockerslides_runners"
  max_size = 3
  min_size = 0
  desired_capacity = 0
  launch_configuration = "${aws_launch_configuration.gitlabrunner_lc.name}"
  vpc_zone_identifier = ["${aws_subnet.runner_sub.id}"]

  tag {
    key = "Name"
    value = "Dockerslides gitlab runner"
    propagate_at_launch = true
  }

  lifecycle {
    ignore_changes = ["desired_capacity"]
  }
}
